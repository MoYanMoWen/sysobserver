
// sysObserverDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "sysObserver.h"
#include "sysObserverDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CsysObserverDlg 对话框

UINT Listen_U(LPVOID lpParam){
	CsysObserverDlg *dlg=(CsysObserverDlg *)lpParam;
	while(1)
	{
		SysCon::GetInstance()->getWin_MemUsage();
		SysCon::GetInstance()->getWin_CpuUsage();
		SysCon::GetInstance()->getWin_DiskUsage();
		int s_cpuRate = SysCon::GetInstance()->getCpuRate();
		CpuRateStruct s_cpuUse = SysCon::GetInstance()->getCpuUse();
		DiskStruct s_diskDetail = SysCon::GetInstance()->getDiskDetail();
		dlg->showCpuRate(s_cpuRate);
		dlg->showCpu(s_cpuUse.s_cpu);
		dlg->showCpuidle(s_cpuUse.s_cpuidle);
		dlg->showDiskCount(s_diskDetail.s_diskCount);
		dlg->showDiskInfo(s_diskDetail.s_diskDetail);
		Sleep(300*10000);
	}
	return 0;
}

void CsysObserverDlg::showCpuRate(int s_rate)
{
	CWnd* pWnd = GetDlgItem(IDC_MEMRATE);
	CString s_tmp;
	s_tmp.Format("%d",s_rate);
	string s_tmp_str;
	s_tmp_str.append(s_tmp);
	s_tmp_str.append("%");
	pWnd->SetWindowText(_T(s_tmp_str.c_str()));
}

void CsysObserverDlg::showCpu(long long s_cpu)
{
	CWnd* pWnd = GetDlgItem(IDC_CPU);
	CString s_tmp;
	s_tmp.Format("%ld",s_cpu);
	string s_tmp_str;
	s_tmp_str.append(s_tmp);
	s_tmp_str.append("%");
	pWnd->SetWindowText(_T(s_tmp_str.c_str()));
}

void CsysObserverDlg::showCpuidle(long long s_cpuidle)
{
	CWnd* pWnd = GetDlgItem(IDC_CPUIDLE);
	CString s_tmp;
	s_tmp.Format("%ld",s_cpuidle);
	string s_tmp_str;
	s_tmp_str.append(s_tmp);
	s_tmp_str.append("%");
	pWnd->SetWindowText(_T(s_tmp_str.c_str()));
}

void CsysObserverDlg::showDiskCount(int s_diskCount)
{
	CWnd* pWnd = GetDlgItem(IDC_DISKCOUNT);
	CString s_tmp;
	s_tmp.Format("%d",s_diskCount);
	string s_tmp_str;
	s_tmp_str.append(s_tmp);
	pWnd->SetWindowText(_T(s_tmp_str.c_str()));
}

void CsysObserverDlg::showDiskInfo(map<int,OneDisk> s_diskDetail)
{
	CWnd* pWnd = GetDlgItem(IDC_DISKINFO);
	map<int,OneDisk>::iterator itor;
	string s_tmp_str;
	for(itor = s_diskDetail.begin();itor!=s_diskDetail.end();itor++)
	{
		s_tmp_str.append("盘符");
		CString s_tmp;
		s_tmp.Format("%d",itor->first);
		s_tmp_str.append(s_tmp);
		s_tmp_str.append(":\r\n");
		s_tmp_str.append("名称:");
		s_tmp_str.append("\r\n");
		s_tmp_str.append(itor->second.s_name);
		s_tmp_str.append("\r\n");
		s_tmp_str.append("类型:");
		s_tmp_str.append("\r\n");
		s_tmp_str.append(itor->second.s_type);
		s_tmp_str.append("\r\n");
		s_tmp_str.append("磁盘总容量:");
		s_tmp_str.append("\r\n");
		s_tmp.Empty();
		s_tmp.Format("%fMB",itor->second.s_i64TotalBytes);
		s_tmp_str.append(s_tmp);
		s_tmp_str.append("\r\n");
		s_tmp_str.append("磁盘剩余空间:");
		s_tmp_str.append("\r\n");
		s_tmp.Empty();
		s_tmp.Format("%fMB",itor->second.s_i64FreeBytesToCaller);
		s_tmp_str.append(s_tmp);
		s_tmp_str.append("\r\n");
		s_tmp_str.append("===============");
	}
	pWnd->SetWindowText(_T(s_tmp_str.c_str()));
}


CsysObserverDlg::CsysObserverDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CsysObserverDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CsysObserverDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CsysObserverDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
END_MESSAGE_MAP()


// CsysObserverDlg 消息处理程序

BOOL CsysObserverDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	//SysCon::GetInstance()->getWin_MemUsage();
	//SysCon::GetInstance()->getWin_CpuUsage();
	//SysCon::GetInstance()->getWin_DiskUsage();
	AfxBeginThread(Listen_U,this);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CsysObserverDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CsysObserverDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

