//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by sysObserver.rc
//
#define IDD_SYSOBSERVER_DIALOG          102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDC_CPU                         1001
#define IDC_MEMRATE                     1002
#define IDC_CPUIDLE                     1003
#define IDC_DISKCOUNT                   1004
#define IDC_DISKCOUNT2                  1005
#define IDC_DISKINFO                    1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
