
// sysObserverDlg.h : 头文件
//
#include "define.h"
#pragma once
#include <afxcontrolbars.h>

// CsysObserverDlg 对话框
class CsysObserverDlg : public CDialogEx
{
// 构造
public:
	CsysObserverDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_SYSOBSERVER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持
public:
	void showCpuRate(int s_rate);
	void showCpu(long long s_cpu);
	void showCpuidle(long long s_cpuidle);
	void showDiskCount(int s_diskCount);
	void showDiskInfo(map<int,OneDisk> s_diskDetail);
// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
};
