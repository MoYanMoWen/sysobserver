#include <iostream>
#include <windows.h>
#include <map>
#include <string>
using namespace std;
struct CpuRateStruct{
	long long s_cpu;
	long long s_cpuidle;
};
struct OneDisk{
	string s_name;
	string s_type;
	float s_i64TotalBytes;
	float s_i64FreeBytesToCaller;
};
struct DiskStruct{
	int s_diskCount;
	map<int,OneDisk> s_diskDetail;
	void clear(){
		s_diskDetail.clear();
	}
};

/// 控制单例
class SysCon
{
public:
	SysCon();
	~SysCon();
	static SysCon* GetInstance()
	{
		static SysCon m_instance;
		return &m_instance;
	}
	/// 获得内存使用-百分比
	void getWin_MemUsage();
	//WIN CPU使用情况  
	void getWin_CpuUsage();
	//获取 WIN 硬盘使用情况  
	void getWin_DiskUsage();
	int getCpuRate(){
		return this->m_cpuRate;
	}
	CpuRateStruct getCpuUse(){
		return this->m_cpuUse;
	}
	DiskStruct getDiskDetail(){
		return this->m_diskDetail;
	}
private:
	int m_cpuRate;
	CpuRateStruct m_cpuUse;
	DiskStruct m_diskDetail;
	__int64 CompareFileTime(FILETIME time1, FILETIME time2);
};